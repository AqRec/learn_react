## STUDY NOTES

#### environment
install them in vscode:
```
ES7 Ract/Redux/GraphQL/React-native snippets
live server
```

install into Chrome from `Google Web Store`:
```
React Developer Tools
```

get react here: [official site](https://reactjs.org/docs/cdn-links.html)
get babel here: [babeljs.io -> setup -> in-the-browser ](https://babeljs.io/setup#installation)
```
    <script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
```

#### 09:
create a form
1. use `e.preventDefault` to prevent page-refresh on submit
```html
    <div id="app"></div>

    <script type="text/babel">
        class App extends React.Component {
            state = {
                name: "Raven",
                age: 30,
            }
            handleChange = (e) => {
                this.setState({
                    name: e.target.value,
                })
            }
            handleSubmit = (e) => {
                e.preventDefault();
                // page will be refreshed on form submitted,
                // use preventDefault to prevent the refreshing.
                console.log("submitted name:", this.state.name)
            }
            render() {
                return (
                    <div className="app-content">
                        <h1>My name is {this.state.name}</h1>
                        <form onSubmit={this.handleSubmit}>
                            <input type="text" onChange={this.handleChange}/>
                            <button>submit</button>
                        </form>
                    </div>
                )
            }
        }
        ReactDOM.render(<App />, document.getElementById("app"));
    </script>
```


#### 03, 04, 05, 06, 07, 08:

1. only `one` root element in render function
2. use `className` instead of `class` for html element tags,
   for `class` in javascript is a reserved keyword.
3. use `type="text/babel"` to translate modern javascript into sth html-acceptable
4. use `{/*  */}` to comment in JSX
5. remember to use arrow function for access to `this`
```html
    <div id="app"></div>
    <script type="text/babel">
        class App extends React.Component { 
            age_begin = 30;
            state = {
                name: "Raven",
                age: this.age_begin,
            }
            handleClick = (e) => { // input is an event object
                // console.log(e.target);
                this.setState({
                    name: this.state.name + "*",
                    age: this.state.age + 1,
                })
            }
            handleMouseOver = (e) => {
                console.log(e.target, e.pageX);
                this.setState({
                    name: "Raven",
                    age: this.age_begin,
                })
            }
            handleCopy = (e) => {
                console.log("try being original for once.");
            }
            render() {
                return (
                    <div className="app-content">
                        <h1>Hello world</h1>
                        {/*<p>{ Math.random()*10 }</p>*/}
                        <p>My name is { this.state.name }</p>
                        <p>I am {this.state.age} years old.</p>
                        <button onClick={this.handleClick}>Click Me</button>
                        <button onMouseOver={this.handleMouseOver}>Hover Me</button>
                        <p onCopy={this.handleCopy}>What we think, we become.</p>
                    </div>
                )
            }
        }
        ReactDOM.render(<App />, document.getElementById("app"));
    </script>
```


#### 02:

DOM: Document Object Model

Components look like HTML, but they are JSX(Javascript XML), they have javascript code in it. <br>
they can contain 'state'(which are data or UI state) and javascript code for functionality.


